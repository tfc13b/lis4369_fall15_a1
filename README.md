﻿# Assignment 1 #


### Git commands ###

1. git init - used to make an existing directory of content into a new GIT repository
2. git status - used to see if anything has been modified and/or staged since your last commit so you can decide if you want to commut a 
a new snapshot and what will be recorded in it 
3. git add -  on a file when you want to include whatever changes you've made to it in your next commit snapshot.
4. git commit - record the snapshot of your staged content. This snapshot can then be compared, shared and reverted to if you need t
5. git push -update a remote repository with the changes you've made locally
6. git pull - synchronize your repository with a remote repository, fetching all the data it has that you do not into branch references locally for merging
7. git reset -  undo the last commit, unstage files that you previously ran


### Multiple Choice  ###

1. What does the following code do?
txtMonthlyInvestment.Focus();
calls the Focus method of the txtMonthlyInvestment control.
2. A class is
a. code (blueprint or framework) that defines the characteristics of an object
3. A data tip
b. displays the value of a variable or property
4. A syntax error is identified in
the Code Editor window and the Error List window.

5. An object is
d. an instance of a class

6. Blocks of code are enclosed in

c. braces - { }

7. C# statements must end with a
a. semicolon - ;

8. Instantiation is the process of generating
a. an object from a class


9. One common cause of a runtime error is
a. a user entry that can’t be converted to a number



10. To refer to a property of an object in your C# code, you code the

c. object name followed by a period and the property name


11. To say that a C# application is event-driven means that it responds to

b. user events and other types of events


12. What is another name for a runtime error?

b. exception


13. What is the term for a method that responds to events?
b. event handler

14. When a running application encounters a problem that prevents a statement
from being executed,

b. a runtime error occurs


15. When you test an application, your goal is to
a. find runtime errors


16. Which of the following examples is a valid C# comment?

d. // This is a comment

17. Which of the following is not a recommended way to improve the readability
of your C# code?

b. Use all capital letters for variable names.

18. Which of the following is not defined by a class?

c. notices



19. Which of the following statements is true?

c. Comments can be inaccurate or out of date.


20. You can get context-sensitive help information from the Code Editor by

d. positioning the insertion point in a keyword and pressing F1

21. How many bits are in one byte?

d. 8

22. How many bytes are stored in the C# long data type?
a. 8


23. Select the following data types in order of increasing size:

b. sbyte < short < int < long


24. Indicate the correct initialization of the follow variables ‘a’ and ‘b’:

c. int a = 12; int b = 20;

### VS downlaod ###

![vs.png](https://bitbucket.org/tfc13b/lis4369_fall15_a1/raw/master/images/2723067801-vs.png)